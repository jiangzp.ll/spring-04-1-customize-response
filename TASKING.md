### 2.1 没有返回值的 Response
1. should return 200 when request method return void.
### 2.2 `@ResponseStatus` 标记。
1. should return 204 when request custom response code.
### 2.3 直接返回字符串
1. should return 200 and message.
### 2.4 直接返回对象
1. should return 200 and message object.
### 2.5 直接返回对象 + @ResponseStatus 标记
1. should return 202 and message object.
### 2.6 使用 `ResponseEntity`
should return specify response entity.
