package com.twuc.webApp.controller;

import com.twuc.webApp.entity.Message;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class UserController {

    @GetMapping("/no-return-value")
    void getVoid() {

    }

    @GetMapping("/no-return-value-with-annotation")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void returnValueWithAnnotation() {

    }

    @GetMapping("/messages/{message}")
    String getMessage(@PathVariable String message) {
        return message;
    }

    @GetMapping("/message-objects/{message}")
    Message getMessageWithClass(@PathVariable String message) {
        return new Message(message);
    }

    @GetMapping("/message-objects-with-annotation/{message}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    Message getMessageObjectWithStatus(@PathVariable String message) {
        return new Message(message);
    }

    @GetMapping("/message-entities/{message}")
    ResponseEntity<Message> getName(@PathVariable String message) {
        return ResponseEntity.status(200)
                .header("X-Auth", "me")
                .contentType(MediaType.APPLICATION_JSON)
                .body(new Message(message));
    }


}
