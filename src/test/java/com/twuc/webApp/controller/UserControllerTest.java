package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_200_when_request_method_return_void() throws Exception {
        mockMvc.perform(get("/api/no-return-value"))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_204_when_request_custom_response_code() throws Exception {
        mockMvc.perform(get("/api/no-return-value-with-annotation"))
                .andExpect(status().is(204));
    }

    @Test
    void should_return_200_and_message() throws Exception {
        mockMvc.perform(get("/api/messages/hello"))
                .andExpect(status().is(200))
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(content().string("hello"));
    }

    @Test
    void should_return_200_and_message_object() throws Exception {
        mockMvc.perform(get("/api/message-objects/tom"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.name").value("tom"))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_UTF8));

    }

    @Test
    void should_return_202_and_message_object() throws Exception {
        mockMvc.perform(get("/api/message-objects-with-annotation/tom"))
                .andExpect(status().is(202))
                .andExpect(jsonPath("$.name").value("tom"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    void should_return_specify_response_entity() throws Exception {
        mockMvc.perform(get("/api/message-entities/tom"))
                .andExpect(status().isOk())
                .andExpect(header().string("X-Auth", "me"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value("tom"));
    }
}
